<?php
return [
    'bca' => [
        'title' => 'BCA',
        'bank' => 'BCA',
        'name' => 'PT.Toledeh',
        'number' => '989899929'
    ],
    'bni' => [
        'title' => 'BNI',
        'bank' => 'BNI',
        'name' => 'PT.Toledeh',
        'number' => '898989889'
    ],
    'mandiri' => [
        'title' => 'Mandiri',
        'bank' => 'Mandiri',
        'name' => 'PT.Toledeh',
        'number' => '23223232'
    ],
    'atm-bersama' => [
        'title' => 'ATM Bersama',
        'bank' => 'Mandiri',
        'name' => 'PT Toledeh',
        'number' => '23223232'
    ],
];
